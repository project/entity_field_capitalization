<?php

namespace Drupal\entity_field_capitalization\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EntitycapitalizationConfigForm.
 */
class EntityCapitalizationConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_field.capitalization_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_capitalization_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_field.capitalization_config');
    $form['exclude_strings'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Excluded Strings'),
      '#description' => $this->t('Comma separated list of string which you do not want to Capitalize, like (iPod,jQuery)'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('exclude_strings'),
    ];

    $form['entity_and_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Content types and fields'),
      '#description' => $this->t('Comma separated list of machine names. One list per line, like ENTITY_TYPE,BUNDLE,FIELD_NAME (node,article,title)'),
      '#default_value' => $config->get('entity_and_fields'),
      '#cols' => 60,
      '#rows' => 10,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('entity_field.capitalization_config')
      ->set('exclude_strings', $form_state->getValue('exclude_strings'))
      ->set('entity_and_fields', $form_state->getValue('entity_and_fields'))
      ->save();
  }

}
