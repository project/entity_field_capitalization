<?php

namespace Drupal\entity_field_capitalization;

/**
 * Interface CapitalizationInterface to declare all the functions.
 */
interface CapitalizationInterface {

  /**
   * Returns list of excluded words from uppercase config.
   *
   * @return array
   *   Get all the list of excluded string.
   */
  public function getExcludeList();

  /**
   * Returns list of fields involved in uppercase for an entity bundle.
   *
   * @param string $entity_type
   *   Entity type of the entity.
   * @param string $bundle
   *   Bundle name of the entity.
   *
   * @return array
   *   Get all the fields of entity to capitalize.
   */
  public function getCapitalizationFields($entity_type, $bundle);

  /**
   * Adds list of terms to the exclude.
   *
   * @param array $exclude_list
   *   Exclude list of string which we do not want to capitalize.
   *
   * @return array
   *   All the string which are excluded.
   */
  public function addToExcludeList(array $exclude_list);

  /**
   * Checks if a content type is configured for uppercase.
   *
   * @param string $entity_type
   *   The entity to look for.
   * @param string $bundle
   *   The bundle of the entity to look for.
   *
   * @return bool
   *   True if is present, false otherwhise.
   */
  public function hasEntity($entity_type, $bundle);

  /**
   * Uppercase the value provided. Checks against excluded strings.
   *
   * @param string $value
   *   String value which we want to capitalize.
   *
   * @return string
   *   Capitalized string.
   */
  public function capitalization($value);

}
