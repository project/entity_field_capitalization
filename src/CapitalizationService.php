<?php

namespace Drupal\entity_field_capitalization;

use Drupal\Core\Config\ConfigFactory;

/**
 * Class CapitalizationService to capitalize the string of and field.
 */
class CapitalizationService implements CapitalizationInterface {

  /**
   * Store the entity & fields.
   *
   * @var array
   */
  private $uppercaseItems = [];

  /**
   * The list of excluded strings.
   *
   * @var array
   */
  private $excludedStrings = [];

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a new CapitalizationService object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->config = $config_factory->get('entity_field.capitalization_config');
    $this->parseConfigData();
  }

  /**
   * {@inheritdoc}
   */
  public function getExcludeList() {
    return $this->excludedStrings;
  }

  /**
   * {@inheritdoc}
   */
  public function getCapitalizationFields($entity_type, $bundle) {
    return $this->uppercaseItems[$entity_type][$bundle] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  private function parseConfigData() {
    $config_data = $this->config->get('entity_and_fields');
    $this->excludedStrings = explode(',', $this->config->get('exclude_strings'));
    $lines = preg_split("/\\r\\n|\\r|\\n/", $config_data);
    $result = [];

    foreach ($lines as $line) {
      $line_list = explode(',', $line);
      $entity_type = array_shift($line_list);
      $bundle = array_shift($line_list);
      $result[$entity_type][$bundle] = $line_list;
    }
    $this->uppercaseItems = $result;
  }

  /**
   * {@inheritdoc}
   */
  public function addToExcludeList(array $exclude_list) {
    $this->excludedStrings = $exclude_list;
  }

  /**
   * {@inheritdoc}
   */
  public function hasEntity($entity_type, $bundle) {
    return (!empty($this->uppercaseItems[$entity_type][$bundle]));
  }

  /**
   * {@inheritdoc}
   */
  public function capitalization($value) {
    $excludedStrings = $this->getExcludeList();
    $regex = "/[\pL\pM\pN][\pL\pM\pN']*/u";
    return preg_replace_callback(
          $regex, function ($matches) use ($excludedStrings) {
              return !in_array($matches[0], $excludedStrings, FALSE) ?
              $this->mbUcfirst($matches[0]) :
              $matches[0];
          }, $value
      );
  }

  /**
   * Helper function to provide a UTF-8 version of ucfirst()
   *
   * @param string $s
   *   String which we will capitalize.
   *
   * @return string
   *   Capitalized string.
   */
  private function mbUcfirst($s): string {
    return mb_convert_case(mb_substr($s, 0, 1), MB_CASE_UPPER, 'UTF-8') . mb_substr($s, 1);
  }

}
