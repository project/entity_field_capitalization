# Entity Field Capitalization

Entity field capitalization module allow us to Capitalize
(first letter of word should be in capital letter) the 
value of any entity filed.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/entity_field_capitalization).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/entity_field_capitalization).

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure the admin toolbar tools at (/admin/config/field-capitalization-settings).

## Maintainers

Current maintainers:

- [Dharmendra Singh](https://www.drupal.org/u/dharmendras)

Supporting organizations:

- [dharmendra](https://www.drupal.org/u/dharmendras) Created this module for you!
